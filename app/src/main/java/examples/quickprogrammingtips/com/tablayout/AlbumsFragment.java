package examples.quickprogrammingtips.com.tablayout;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class AlbumsFragment extends Fragment {
    final static String PARAMETERONE = "parameterone";
    final static String PARAMETERTWO = "parametertwo";
    private TextView showReceivedData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_browse, container, false);
        showReceivedData = (TextView) view.findViewById(R.id.showReceivedData);
        Log.v("AlbumsFragment","create");
        return inflater.inflate(R.layout.fragment_browse, container, false);
    }
    //f

    @Override
    public void onStart() {
        super.onStart();
        Log.v("AlbumsFragment", "onstart");
    }
}
